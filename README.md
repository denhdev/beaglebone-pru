# BeagleBone - Programmable Real-Time Unit (PRU)
Examples how to develop for the PRU-ICSS of the BeagleBone Black by using the CLPRU compiler.

- **Remote Processor Framework Basics:**
    This example shows how to develop for the PRU-ICSS by using the clpru compiler. 

- **Remote Processor Messaging Framework (RPMsg) example:**
    A basic RPMsg example. It explains a method of message passing between the ARM system and the PRU-ICSS.
    
- **Shared Memory example:** 
    An example to use the OCP port to access the shared memory of the PRU-ICSS. 
    
- **Interrupt example:** 
    Example usage for an interrupt by using the eCAP module of the PRU-ICSS.

- **PWM Generator example:** 
    A firmware which generates a PWM signal by using a general-purpose output pin. 
    
- **Serial Data Transfer via General-Purpose Input / Output:** 
    This application transfers data between both PRU cores by using the general-purpose pin capabilities. 
    This example is only a proof of concept implementation of a serial data transfer via GPIO.