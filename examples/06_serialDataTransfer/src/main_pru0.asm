;-----------------------------------------------------------------------------------------------------------------------
; GPO Signal Generating Loop |
;----------------------------
; This firmware generates a signal with encoded information
; on the BeagleBone Black at pin 28 on header 9.
; The application is designed to be as quick as possible.
; The data will be sent only one time reliable.
; Author: Dennis Hofmann
; Date:   2017-11-29
;-----------------------------------------------------------------------------------------------------------------------
; Register usage |
;-----------------
;R30:	General-purpose output interface
;R17:   Holds the data to send
;R16:	Next bit counter. Value indicates the bit to sent
;R15:	Loop counter
;R23:	Preload loop counter value

	.text
	.global main
	.retain
;-----------------------------------------------------------------------------------------------------------------------
;Init |
;------
SIGNAL_PERIOD   .set 1000                     ;Sends time in ns/5! Send frequency 1/t=f
DATA        	.set 00010100111000111001b    ;Data to transfer

main:
   	CLR R30, R30, 3
    LDI32 R17, DATA            ;Loads data to register
    LDI32 R16, 0               ;Data bit count register

	LDI32 r23, SIGNAL_PERIOD
	SUB r23, r23, 6            ;Removes cycles used by the application beside the wait loop


;-----------------------------------------------------------------------------------------------------------------------
;Signal generation loop|
;-----------------------
selectState:                   ;Selects the state for the next data bit
	QBBS toggle_high, R17, R16

toggle_low:
	CLR	R30, R30, 3            ;Sets BBB header pin P9.28 to low
	QBA wait

;Toggle pin
toggle_high:
	SET	R30, R30, 3            ;Sets BBB header pin P9.28 to high
    QBA wait                   ;NOP instruction

;Wait loop to toggle with a specific frequency
wait:
	MOV R15, R23               ;Loads loop counter

Wait_loop:
	SUB	R15, R15, 2
	QBLT Wait_loop, R15, 0     ;Can lead to a hangup, if the frequency is odd! Find another solution!

	ADD R16, R16, 1            ;Increases the data bit counter
	QBA selectState



;-----------------------------------------------------------------------------------------------------------------------
;Stop |
;------
stop:
	HALT



;------------------------------------------------------------
;Resource table section
;Remoteproc needs this section to load the firmware file to the PRU
;It contains only a version number for remoteproc
;-----------------------------------------------------------
	.sect ".resource_table", RW
	.retain                         ;Important to prevent the assembler from removing this section
remoteproc_ResourceTable:
	.word 1, 0, 0 ,0 ,0
