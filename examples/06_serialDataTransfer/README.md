# Serial Data Transfer via General-Purpose Input / Output (GPIO)
This application transfers data between both PRU cores by using the general-purpose pin capabilities. 
PRU0 generate a signal and encode bits on it. 
PRU1 receive the signal and stops after a specific bit order is received. 
The PRU1 firmware will store all the received bits in the shared memory of the PRU-ICSS. 
This data can be accessed and displayed by the ARM application. 
This example is only a proof of concept implementation of a serial data transfer via GPIO.



# Assembling / Compiling
Both PRU core firmware files can be assembled and linked by using *make*. 
The same *make* rule will also compile the ARM application.

	make sdt -> Create the firmware and the ARM application
	make clean -> Clean up


# Install
*make* can be used to copy the firmware and overlay to the  **/lib/firmware/** directory.

	sudo make install


# Pin Multiplexing
The *bone_capemgr* can be used to load the device tree overlay as shown below:

	sudo sh -c "echo PRU-GPIO-SDT > /sys/devices/platform/bone_capemgr/slots"

mcASP uses the pin 28 on header 9 by default. 
The pin must be released first before the multiplexing can take place.

## Check the Pin Multiplexing
The used pins have the internal pin numbers 103 and 96. 

	sudo sh -c "cat /sys/kernel/debug/pinctrl/44e10800.pinmux/pins"
	cat /sys/devices/platform/bone_capemgr/slots
	
# Usage
The first step is to start the receiving PRU core.
Also, the LED0 has to be released from the usage as a heartbeat before the sending PRU core starts. 
The *start.sh* script can be used to run the receiving PRU core and release the LED0.

	sudo ./start.sh

The sending PRU core can be started now as shown below:

	sudo sh -c "echo 'start' > /sys/class/remoteproc/remoteproc1/state"

Finally, after the user LED0 lights up, the ARM application can print the data to the terminal.
	
	sudo ./out/main_arm 


