;-----------------------------------------------------------------------------------------------------------------------
; PWM Generator		  |
;----------------------
; This firmware for the PRU-ICSS generates a PWM signal.
; It starts and updates after system event 16 appeared on Host 0.
; The data to configure the PWM will be load from memory address 0x10000
; Author: Dennis Hofmann
; Date:   2017-11-29
;-----------------------------------------------------------------------------------------------------------------------
	.text                      ;Indicates a text section
	.retain
	.global main

INTC_REG_SICR   .set 0x0024     ;System Event Status Indexed Clear Register
INTC_REG_EISR   .set 0x0028     ;System Event Enable Indexed Set Register
INTC_REG_HIEISR .set 0x0034
INTC_REG_GER    .set 0x0010
INTC_REG_SECR0  .set 0x0280     ;System Event Status Enabled/Clear Register0
INTC_REG_CMR0   .set 0x0400     ;Channel map register 0
INTC_REG_CMR3   .set 0x040C
INTC_REG_HMR0   .set 0x0800     ;Host map register 0
INTC_REG_HIPIR0 .set 0x0900     ;Host Interrupt Prioritized Index Register0
INTC_REG_HIPIR1 .set 0x0904
INTC_REG_SIPR0  .set 0x0D00     ;System event polarity register
INTC_REG_HIER   .set 0x1500     ;Host Interrupt Enable Registers
sMem			.set 0x10000
PIN_SIGNAL		.set 15			;Output pin number


main:
	LDI32 R30, 0                ;Sets all pins low

;-----------------------------------------------------------------------------------------------------------
;INTC configuration |
;-------------------
;C0 = Constant table entry for the INTC
;R14 = value
;R15 = offset
;-----------------------------------------------------------------------------------------------------------
;Sets polarity register |
;-----------------------
	LDI32 R15, INTC_REG_SIPR0     ;Loads the SIPR0 register offset
	LDI32 R14, 0xFFFFFFFF         ;Set all bits to 1
	SBCO &r14, C0, R15, 4         ;Map the channel to the host

;------------------------
;Sets the type registers |
;------------------------
	LDI32 R14, 0x0
	LDI32 R15, 0xD80
	SBCO &R14, C0, R15, 4
	LDI32 R15, 0xD84
	SBCO &R14, C0, R15, 4

;-----------------------------------
; Maps system event 16 to channel 0 |
;-----------------------------------
	LDI32 R15, INTC_REG_CMR0      ;Loads the CMR0 register offset
	ADD R15, R15, 16              ;Locates the right offset for the system event
	LDI32 R14, 0                  ;Loads the channel number
	SBCO &r14.b0, C0, r15, 1      ;Maps the channel to a system event

;---------------------------
; Maps channel 0 to host 0 |
;---------------------------
	LDI32 R14, 0                   ;Loads the host number
	LDI32 R15, INTC_REG_HMR0       ;Loads the register offset
	ADD R15, R15, 0                ;Adds the channel number to the offset
	SBCO &r14.b0, C0, R15, 1       ;Maps the channel by writing the host

;--------------------------
; Clears system event 16  |
;--------------------------
	LDI32 R14, 1<<16               ;Sets bit 16
	LDI32 R15, INTC_REG_SECR0      ;Loads register offset
	SBCO &R14, C0, R15, 4          ;Stores to register

;--------------------------
; Enables system event 16 |
;--------------------------
	LDI32 R14, 16                  ;Loads system event index
	LDI32 R15, INTC_REG_EISR       ;Loads the register offset
	SBCO &R14, C0, R15, 4          ;Enables system event

;-------------------------------
; Enables interrupt for host 0 |
;-------------------------------
	LDI32 R14, 0001b               ;Sets the first bit to enable the host interrupt
	LDI32 R15, INTC_REG_HIER       ;Loads the register offset
	SBCO &R14, C0, R15, 4          ;Sets register

;-----------------------------
;  Enables interrupts global |
;-----------------------------
	LDI32 R14, 1b
	SBCO &R14, C0, INTC_REG_GER, 1


wait:
	QBBS interrupt_handler, R31, 30  ;Checks the Host0 interrupt port
	JMP wait                         ;Waits for the interrupt and a hTime


;-------------------------------------------------------------------------------------------
; PWM signal loop
;-------------------------------------------------------------------------------------------
; R12 = offset / adresses
; R13 = Temp values
; R14 = High time
; R15 = Signal period in ns/5. Each instruction take 5ns
;-------------------------------------------------------------------------------------------
; This loop consumes exactly sFrame cycles.
;-------------------------------------------------------------------------------------------
toggle_high:
	SET R30, R30, PIN_SIGNAL    ;Sets high
	MOV R13, R14                ;Loads the high time counter
	SUB R13, R13, 4             ;Removes 4 cycles because we execute 4 instructions beside the loop in the high phase

delay_loop1:
	SUB R13, R13, 2             ;Removes 2 cycles. Because the loop contains 2 instructions
	QBLT delay_loop1, R13, 0

toggle_low:
	MOV R13, R15                ;Loads the low time counter
	CLR R30, R30, PIN_SIGNAL    ;Sets the pin to low
	SUB R13, R13, 4             ;Removes 4 cycles because we execute 4 instruction beside the loop in the low phase

delay_loop2:
	SUB R13, R13, 2
	QBLT delay_loop2, R13, 0
	QBBS interrupt_handler, R31, 30   ;Checks the Host0 interrupt port
	QBA toggle_high


;----------------------------------------------------------------
; Interrupt handler |
;--------------------
interrupt_handler:
	LDI32 R12, sMem
	LBBO &R14, R12, 4, 4          ;Loads the new high time from the shared memory
	LBBO &R15, R12, 8, 4          ;Loads the signal period in loops to dealay
	SUB R15, R15, R14             ;Calculates the low cycles

;--------------------------
; Clears system event 16  |
;--------------------------
	LDI32 R13, 1<<16              ;Sets bit 16
	LDI32 R12, INTC_REG_SECR0     ;Loads the register offset
	SBCO &R13, C0, R12, 4         ;Stores to register

;--------------------------
; Enables system event 16 |
;--------------------------
	LDI32 R13, 16                 ;Loads the system event index
	LDI32 R12, INTC_REG_EISR      ;Loads the register offset
	SBCO &R14, C0, R12, 4         ;Enables the system event

;-------------------------------
; Enables interrupt for host 0 |
;-------------------------------
	LDI32 R13, 0001b               ;Sets the first bit to enable the host interrupt
	LDI32 R12, INTC_REG_HIER       ;Loads the register offset
	SBCO &R13, c0, R12, 4          ;Sets the register

;----------------------------
; Enables interrupts global |
;----------------------------
	LDI32 R13, 1b
	SBCO &R13, C0, INTC_REG_GER, 1

	JMP toggle_high				;Starts signal generation


;------
; End |
;------
stop:
	HALT



;------------------------------------------------------------
;Resource table section
;Remoteproc needs this section to load the firmware file to the PRU
;It contains only a version number for remoteproc
;-----------------------------------------------------------
	.sect ".resource_table", RW
	.retain                         ;Important to prevent the assembler from removing this section
remoteproc_ResourceTable:
	.word 1, 0, 0 ,0 ,0
