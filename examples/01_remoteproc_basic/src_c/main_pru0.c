/*******************************************************************************
 *  Remoteproc C example      *
 ******************************
 A basic application to be compiled with clpru.
 The register R30[15] will be toggled with the given
 DELAY_LOOPS delay until R31[14] will be set to high.
 Delay loop calculation:  DELAY_LOOPS*5ns=delay in ns
 Author: Dennis Hofmann
 Date:   2017-11-29
/*******************************************************************************/
#include "resource_table_empty.h"           //Includes a empty resource table

volatile register unsigned __R31;           //Input interface
volatile register unsigned __R30;           //Output interface

#define LED_OUT_PIN (1<<15)
#define BTN_IN_PIN (1<<14)
#define DELAY_LOOPS 200000000


/*******************************************************************************
 * main.c
 *******************************************************************************/
void main(void) {
	__R30 = 0;								//Sets all output pins to low.
	do {
		__R30 |= LED_OUT_PIN;				//Sets pin to high
		__delay_cycles(DELAY_LOOPS);
		__R30 &= ~LED_OUT_PIN;				//Sets pin to low
		__delay_cycles(DELAY_LOOPS);
	} while(!(__R31 & BTN_IN_PIN));
	__halt();								//Stops the PRU
}