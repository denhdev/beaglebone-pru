# eCAP as Timer Example
Example usage for an interrupt. 
The eCAP-Module will be used to demonstrate the basic usage of additional modules. 
The eCAP module triggers an interrupt to toggle the user LED0 of the BeagleBone Black.


# Assembling
*make* can be used to assemble and link the PRU firmware.

	make eCAP -> Assembling and linking of the firmware
	make clean -> Clean up


# Install
*make* can be used to copy the firmware and overlay to the  **/lib/firmware/** directory.
	
	sudo make install

# Usage
To allow the firmware to use the user LED0 it needs to be freed up. 
By default, the BeagleBone Black heartbeat uses the LED. 
The *start.sh* script can be used to free up the LED0 and start the application.

	sudo ./start.sh -> Load and start the firmware for PRU0
	
Should the firmware be started without the script:

	echo "none" >/sys/class/leds/beaglebone:green:usr0/trigger
	echo 'start' > /sys/class/remoteproc/remoteproc1/state
