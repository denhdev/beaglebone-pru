/*******************************************************************************
 * Example RPMsg ARM Application    *
 *************************************
 An RPMsg example application. The ARM application tries to open a
 channel to PRU0, send a message and wait for the response.
 If the application receives a message, it will send the next one.
 Author: Dennis Hofmann
 Date:   2017-11-29
/*******************************************************************************/
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

#define MAX_BUFFER_SIZE		512
#define NUM_MESSAGES		10
#define CHANNEL_NAME_PRU0	"/dev/rpmsg_pru0"

int main(void) {
	int channelDevice;
	char readBuf[MAX_BUFFER_SIZE];

	//Opens the rpmsg_pru character device file to open the channel.
	//The rpmsg driver create the channel after loading the PRU firmware
	channelDevice = open(CHANNEL_NAME_PRU0, O_RDWR);

	//Checks if the channel is open. Otherwise, stops the application
	if (channelDevice < 0)  {
		printf("Failed to open the channel%s\n", CHANNEL_NAME_PRU0);
		return -1;
	}

	//Send and receive loop
	for (int i=0; i < NUM_MESSAGES; i++) {

		//Sends the message by writing to the channel device file
		if (write(channelDevice, "Hello PRU!", 10) > 0) {
			printf("Message %d: Sent 'Hello PRU!' to PRU0\n", i);
		} else {
			printf("Can not send message %d to PRU0\n", i);
		}

		//Waits for response by reading the channel device file
		if (read(channelDevice, readBuf, MAX_BUFFER_SIZE) > 0) {
			printf("Message %d received from PRU0: %s\n\n", i, readBuf);
		}
		sleep(1);
	}

	close(channelDevice);		//Closes the rpmsg_pru channel device file
}
