;***********************************************************************************************
; eCAP as timer				|
;----------------------------
; A PRU firmware to configure the ECAP as PWM generator and
; use the interrupt triggered by it to toggle the Beaglebone Black user LED0
; Author: Dennis Hofmann
; Date: 21.11.2017
;************************************************************************************************
	.text
	.retain
	.global main

INTC_REG_SICR	.set 0x0024	;System Event Status Indexed Clear Register
INTC_REG_EISR	.set 0x0028	;System Event Enable Indexed Set Register
INTC_REG_HIEISR	.set 0x0034
INTC_REG_GER	.set 0x0010
INTC_REG_SECR0	.set 0x0280	;System Event Status Enabled/Clear Register0
INTC_REG_CMR0	.set 0x0400	;Channel map register 0
INTC_REG_CMR3	.set 0x040C
INTC_REG_HMR0	.set 0x0800	;Host map register 0
INTC_REG_HIPIR0	.set 0x0900	;Host Interrupt Prioritized Index Register0
INTC_REG_HIPIR1	.set 0x0904
INTC_REG_SIPR0	.set 0x0D00	;System event polarity register
INTC_REG_SITR0	.set 0x0D80
INTC_REG_HIER	.set 0x1500	;Host Interrupt Enable Registers
ARM_REG_GPIO_DATAOUT		.set 0x13C
ARM_REG_GPIO_CLEARDATAOUT	.set 0x190
ARM_REG_GPIO				.set 0x4804C000
CFG_REG_SYSCFG	.set 0x0004

main:
;-----------------------------------------------------------------------------------------------------------------------
; ARM configuration |
;--------------------
;R14 = value
;R15 = offset
;C4 = Constant table entry for the PRU-ICSS CFG (local)
;----------------------------
;Enables the OCP master port	|
;----------------------------
	LDI32 R15, CFG_REG_SYSCFG	;Laods the SYSCFG register offset
	LBCO &R14, C4, R15, 4       ;Loads SYSCFG register
	CLR R14, R14, 4          	;Clears bit 4 to enable OCP
	SBCO &R14, C4, R15, 4       ;Stores the change to SYSCFG register

;----------------------------------------------------------------------------
;Deactivates the user LED0 on the ARM by writing to the ARM GPIO register	|
;----------------------------------------------------------------------------
	LDI32 R14, 1<<21             ;Pin 21 is wired to the user LED0 of the ARM
	LDI32 R15, ARM_REG_GPIO | ARM_REG_GPIO_CLEARDATAOUT
	SBBO &R14, R15, 0, 4         ;Writes 1<<21 in to GPIO1 SETDATAOUT


;-----------------------------------------------------------------------------------------------------------------------
;INTC configuration |
;-------------------
;C0 = Constant table entry for the INTC
;r14 = value
;r15 = offset
;----------------------------
;Sets the polarity register |
;----------------------------
	LDI32 R15, INTC_REG_SIPR0	;Loads the SIPR0 register offset
	LDI32 R14, 0x8000			;Loads bit 15
	SBCO &R14, C0, R15, 4		;Sets bit 15 of the SIPR0 register

;------------------------
;Sets the type registers |
;------------------------
	LDI32 R15, INTC_REG_SITR0	;Loads the SITR0 register offset
	LDI32 R14, 0x0				;Loads the value to set the type
	SBCO &R14, C0, R15, 4		;Writes 0 to the SITR0 to use pulse interrupts

;------------------------------------
; Maps system event 15 to channel 1 |
;------------------------------------
	LDI32 R15, INTC_REG_CMR3	;Loads the CMR0 register offset
	ADD	R15, R15, 3				;Locates the right offset for the system event within the register
	LDI32 R14, 1				;Loads the channel number
	SBCO &R14.b0, C0, R15, 1	;Maps the channel to a system event

;---------------------------
; Maps channel 1 to host 1 |
;---------------------------
	LDI32 R15, INTC_REG_HMR0	;Loads the register offset
	LDI32 R14, 1				;Loads the host number
	ADD	R15, R15, 1				;Adds the channel number to the offset to address the right byte
	SBCO &R14.b0, C0, R15, 1	;Maps the channel by writing the host

;--------------------------
; Clears system event 15  |
;--------------------------
	;Clear system event by bit
	LDI32 R14, 1<<15			;Sets bit 15
	LDI32 R15, INTC_REG_SECR0	;Loads SECR register offset
	SBCO &R14, C0, R15, 4		;Stores the value to register

	;Clear system event by index
	;LDI32 R14, 15
	;SBCO &R14,	C0 ,0x24, 4

;-----------------------------------
; Enables the interrupt for host 1 |
;-----------------------------------
	LDI32 R14, 0000000010b		;Sets the second bit for host1
	LDI32 R15, INTC_REG_HIER	;Loads the register offset
	SBCO &R14, c0, R15, 4		;Sets register

	;Enables host by index
	;LDI32 R14,	0               ;Loads the host number
	;LDI32 R15, 0x34            ;HIEISR register offset
	;SBCO &r14,	c0,	R15, 1

;---------------------------
; Enables the system event |
;---------------------------
	LDI32 R14, 15				;Load system event index
	LDI32 R15, INTC_REG_EISR	;Load the register offset
	SBCO &R14, C0, R15, 4		;Enable system event


;---------------------------------------
;Uncomment to trigger a test interrupt |
;---------------------------------------
;	LDI32 R14, 1<<15
;	SBCO &R14, c0,	0x20,	4		;Triggers the system event



;-----------------------------------------------------------------------------------------------------------------------
; eCap configuration |
;---------------------
; C3 = Constant table entry for eCAP
; R14 = value
; R15 = offset
;---------------------------------------
ECAP_REG_TSCTR	.set 0x00
ECAP_REG_CAP3	.set 0x10
ECAP_REG_ECCTL1	.set 0x28
ECAP_REG_ECCTL2	.set 0x2A
ECAP_REG_ECEINT	.set 0x2C
ECAP_REG_ECCLR	.set 0x30

;------------------------
;Clears the counter		|
;------------------------
	LDI32 R14, 0
	LDI32 R15, ECAP_REG_TSCTR      ;Loads TSCTR register offset
	SBCO &R14, c3, R15, 4

;-----------------------
; Loads timer values   |
;-----------------------
	LDI32 R14, 1000000000          ;Sets to 5 secounds.
	LDI32 R15, ECAP_REG_CAP3       ;Loads CAP3 register offset
	SBCO &R14, C3, R15, 4

;-----------------------------
; Configures the eCAP Module |
;-----------------------------
	LDI32 R14, 0b					;Sets ECCTL1
	LDI32 R15, ECAP_REG_ECCTL1      ;Loads ECCTL1 register offset
	SBCO &R14, C3, R15, 2

	;Configure the module as a pwm generator. APWM is getting enabled. 0x2D0
	LDI32 R14, 01011010000b			;Sets ECCTL2
	LDI32 R15, ECAP_REG_ECCTL2      ;Loads ECCTL2 register offset
	SBCO &R14, C3, R15, 2


;----------------------------------------------
; Enables the interrupt sources
;----------------------------------------------
	LDI32 R14, 10000000b			;Counter Equal Compare Interrupt Enable (bit7)
	LDI32 R15, ECAP_REG_ECEINT      ;Loads ECEINT register offset
	SBCO &R14, C3, R15, 2			;Enables interrupts for period & compare

;----------------------------------------------
; Clears the interrupt state
;----------------------------------------------
	LDI32 R14, 0xFF
	LDI32 R15, ECAP_REG_ECCLR       ;Loads ECCLR register offset
	SBCO &R14, C3, R15,	2			;Clears the interrupt state


;----------------------------
; Enables interrupts global |
;----------------------------
	LDI32 R14, 1b
	SBCO &R14, C0, INTC_REG_GER, 1




;-----------------------------------------------------------------------------------------------------------------------
;Main loop - Waits for system event on host 1
;-----------------------------------------------------------------------
wait_for_sys_event:
	QBBS interrupt_handler_host1, r31, 31  ;Checks if a host1 interrupt appeared
	JMP	wait_for_sys_event



;-----------------------------------------------------------------------------------------------------------------------
; Interrupt handler host 1 |
;--------------------------
interrupt_handler_host1:
	LDI32 R15, INTC_REG_HIPIR1              ;Loads the Host Interrupt Prioritized Index Register1 offset
	LBCO &R14, C0, R15, 4                   ;Loads the interrupt with the highredt priority
	QBEQ pr1_ecap_intr_req_handler,	R14, 15	;Jumps if event 15 occurred
	JMP stop                                ;A unknown interrupt appears. That should not happen -> stop the PRU


;---------------------------------------------------
; pru eCAP interrupt handler
; This handler toggles the user LED0
; r20: value register
;---------------------------------------------------
pr1_ecap_intr_req_handler:
;--------------------------------------
;Toggles the user LED0 by using XOR
;--------------------------------------
	LDI32 R20, ARM_REG_GPIO | ARM_REG_GPIO_DATAOUT    ;Loads the register base address
	LBBO &R21, R20, 0, 4                              ;Loads the ARM GPIO output register
	LDI32 R22, 1<<21
	XOR R21, R21, R22
	SBBO &R21, R20,	0, 4

;---------------------------------------------------
;Resets the system event  |
;-------------------------
; Clears the system event 15  |
;-------------------------
	LDI32 R13, 1<<15                   ;Sets bit 16
	LDI32 R12, INTC_REG_SECR0          ;Loads the register offset
	SBCO &R13, C0, R12, 4              ;Stores to register

;------------------------------
; Enables the system event 15 |
;------------------------------
	LDI32 R14, 15                      ;Loads the system event index
	LDI32 R15, INTC_REG_EISR           ;Loads the register offset
	SBCO &R14, C0, R15, 4              ;Enables the system event

;-----------------------------------
; Enables the interrupt for host 1 |
;-----------------------------------
	LDI32 R13, 00010b                  ;Sets the first bit to enable the host interrupt
	LDI32 R12, INTC_REG_HIER           ;Loads the register offset
	SBCO &R13, C0, R12, 4              ;Sets the register

;---------------------------
; Enable interrupts global |
;---------------------------
	LDI32 R13, 1b
	SBCO &R13, c0, INTC_REG_GER, 1

;----------------------------------------------
; Clears the interrupt state
;----------------------------------------------
	LDI32 R14, 0xFF
	LDI32 R15, ECAP_REG_ECCLR       ;Loads the ECCLR register offset
	SBCO &R14, C3, R15,	2			;Clears the interrupt state

	JMP wait_for_sys_event          ;Jumps back to wait for another event




;-----------------------------------------------------------------------------------------------------------------------
; Stop |
;-------
stop:
	HALT



;------------------------------------------------------------
;Resource table section
;Remoteproc needs this section to load the firmware file to the PRU
;It contains only a version number for remoteproc
;-----------------------------------------------------------
	.sect ".resource_table", RW
	.retain					;Important to prevent the assembler to optimze this section and remove the table
remoteproc_ResourceTable:
	.word 1, 0, 0 ,0 ,0
