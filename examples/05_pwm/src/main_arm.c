/*******************************************************************************
 *  PRU Generator Controller  *
 ******************************
 This application controls the PRU firmware by configuring the PWM-generators.
 The data for the period and the high time of a signal will be written to the PRU-ICSS shared memory.
 The system event 16 signalize the firmware to reconfigure themselves.
 Author: Dennis Hofmann
 Date:   2017-11-29
/*******************************************************************************/
#include <stdio.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdint.h>


#define PRU_ADDR 0x4A300000					//PRU-ICSS memory address
#define PRU_SHAREDRAM_OFFSET 0x00010000
#define PRU_INTC_OFFSET 0x00020000
#define PRU_INTC_REG_SISR 8

int main(void) {
	uint32_t* prusharedmemory;
	uint32_t* pruINTC;

	int fd = open("/dev/mem", O_RDWR | O_SYNC);
	prusharedmemory = (uint32_t*) mmap(NULL, 0x10, PROT_READ | PROT_WRITE, MAP_SHARED, fd, PRU_ADDR + PRU_SHAREDRAM_OFFSET);
	pruINTC = (uint32_t*) mmap(NULL, 0x1500, PROT_READ | PROT_WRITE, MAP_SHARED, fd, PRU_ADDR + PRU_INTC_OFFSET);

	if(!prusharedmemory || !pruINTC) printf("Cannot map memory!");

	int input;
	while(1) {
		printf("Please enter a angle in degree\n");
		scanf("%d", &input);
		if((input >= 0) && (input <= 90)) {
			int angle = ((200000 / 90) * input) + 200000; 	//angle=((signalCycleRange / 90) * input) + minCycles
			angle = ((angle / 100) * 100);
			prusharedmemory[1] = angle;
			prusharedmemory[2] = 10000000;					//Period time in cycles

			pruINTC[PRU_INTC_REG_SISR] = 16;	//Triggers the system event by writing the index to the PRU INTC SISR register
		}
	}
}