/*******************************************************************************
 *  Example RPMsg PRU0 Firmware      *
 *************************************
 An RPMsg example application. The Firmware for PRU0 initialize the system and wait for a message from the ARM.
 If the message will be received, it gets send Back after a small modification of the Text.
 Author: Dennis Hofmann
 Date:   2017-11-29
/*******************************************************************************/
#include <stdint.h>
#include <pru_intc.h>
#include <pru_rpmsg.h>
#include "resource_table_0.h"

#define VIRTIO_CONFIG_S_DRIVER_OK 4
#define HOST_INT 0x40000000
#define CHAN_NAME "rpmsg-pru"
#define CHAN_DESC "Channel PRU0"
#define CHAN_PORT 0

volatile register unsigned __R31;

#define TO_ARM_SYSEVT 16
#define FROM_ARM_SYSEVT 17

unsigned char payload[RPMSG_BUF_SIZE];

/****************************************************************
 * main function
 ****************************************************************/
void main(void) {
	struct pru_rpmsg_transport transport;
	unsigned short src, dst, len;
	volatile unsigned char *status;

	/////////////////////////////////////////////
	//rpmsg initialisation
	/////////////////////////////////////////////
	CT_INTC.SICR_bit.STS_CLR_IDX = FROM_ARM_SYSEVT;                //Clears the system event, which should arrive from the ARM

	//Checks if the VRING was configured successfully
	status = &resourceTable.rpmsg_vdev.status;
	while (!(*status & VIRTIO_CONFIG_S_DRIVER_OK));

	if (pru_rpmsg_init(&transport, &resourceTable.rpmsg_vring0, &resourceTable.rpmsg_vring1, TO_ARM_SYSEVT, FROM_ARM_SYSEVT) == PRU_RPMSG_SUCCESS) {
		if (pru_rpmsg_channel(RPMSG_NS_CREATE, &transport, CHAN_NAME, CHAN_DESC, CHAN_PORT) == PRU_RPMSG_SUCCESS) {

			//////////////////////////////////////////////////////////////////////////////////
			//Main loop. Waits for a message from the ARM and send the payload back to it
			//////////////////////////////////////////////////////////////////////////////////
			while (1) {
				if (__R31 & HOST_INT) {
					//Resets the system event after a event appear
					CT_INTC.SICR_bit.STS_CLR_IDX = FROM_ARM_SYSEVT;

					//Receives all messages on the query
					while (pru_rpmsg_receive(&transport, &src, &dst, payload, &len) == PRU_RPMSG_SUCCESS) {
						payload[len - 4] = 65;
						payload[len - 3] = 82;
						payload[len - 2] = 77;
						pru_rpmsg_send(&transport, dst, src, payload, len);
					}

				}
			}
		}
	}
}
