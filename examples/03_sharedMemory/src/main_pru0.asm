;-----------------------------------------------------------------------------------------------------------------------
; Shared memory example  |
;-------------------------
; This firmware uses macros to enable/disable the user LED0 on the BeagleBone Black.
; Also, the program stops the PRU execution and write to the shared memory.
; Author: Dennis Hofmann
; Date:   2017-11-29
;-----------------------------------------------------------------------------------------------------------------------
; Register usage |
;-----------------
;R20:	Addresses
;R21:	Temporary values
	.text
	.retain
	.global main

ARM_REG_GPIO				.set 0x4804C000			;ARM GPIO Register base address
ARM_REG_GPIO_CLEARDATAOUT	.set 0x190				;CLEARDATAOUT register offset
ARM_REG_GPIO_SETDATAOUT		.set 0x194				;SETDATAOUT register offset

;-----------------------------------------------------------------------------------------------------------------------
;Enables the user LED0. The LED is connected to pin 21
;-----------------------------------------------------------------------------------------------------------------------
USERLED0_ENABLE	.macro
	LDI32 R21, 1<<21                                         ;Loads bit
	LDI32 R20, ARM_REG_GPIO | ARM_REG_GPIO_SETDATAOUT        ;Loads the register address
	SBBO &R21, R20, 0, 4                                     ;Enables the pin by writing to ARM_REG_GPIO_SETDATAOUT
	.endm


;-----------------------------------------------------------------------------------------------------------------------
; Disables the user LED 0
;-----------------------------------------------------------------------------------------------------------------------
USERLED0_DISABLE	.macro
	LDI32 R21, 1<<21
	LDI32 R20, ARM_REG_GPIO | ARM_REG_GPIO_CLEARDATAOUT
	SBBO &R21, R20, 0, 4
	.endm


;-----------------------------------------------------------------------------------------------------------------------
; Breakpoint macro
; Write a value to the shared memory and wait for the ARM to read it
;-----------------------------------------------------------------------------------------------------------------------
BREAKPOINT	.macro value
;-----------------------------
; Sets the value and bitflag |
;-----------------------------
	LDI32 R20, 0x10000          ;Loads the memory location
	LDI32 R21, value            ;Loads the value
	SBBO &R21, R20, 4, 4        ;Stores the value
	LDI32 R21, 01b              ;Sets the bitflag
	SBBO &R21, R20, 0, 4        ;Stores the bitflag
	USERLED0_ENABLE             ;Enables the user LED0 to signalise the break

;-----------------------------------------
; Waits for the ARM to clear the bitflag |
;-----------------------------------------
$wait?:
	LDI32 R20, 0x10000          ;Loads the memory location
	LBBO &R21, R20, 0, 1
	QBBS $wait?, R21, 0
	USERLED0_DISABLE
	.endm


;-----------------------------------------------------------------------------------------------------------------------
; DEBUGREG macro
; Writes a register value to the shared memory and waits for the ARM to read it
;-----------------------------------------------------------------------------------------------------------------------
DEBUGREG   .macro value
;---------------------------------
; Sets the value and the bitflag |
;---------------------------------
	LDI32 R20, 0x10000           ;Loads memory location
	MOV R21, value               ;Loads the value
	SBBO &R21, R20, 4, 4         ;Stores the value
	LDI32 R21, 01b               ;Sets the bitflag
	SBBO &R21, R20, 0, 4         ;Stores the bitflag
	USERLED0_ENABLE              ;Enables the LED, to signalise the break

;-----------------------------------------
; Waits for the ARM clearing the bitflag |
;-----------------------------------------
$wait?:
	LDI32 R20, 0x10000           ;Loads the memory location
	LBBO &R21, R20, 0, 1
	QBBS $wait?, R21, 0
	USERLED0_DISABLE
	.endm


;-----------------------------------------------------------------------------------------------------------------------
; Main loop
;-----------------------------------------------------------------------------------------------------------------------
main:

;-----------------------------
;Enables the OCP master port |
;-----------------------------
	LBCO &R21, C4, 4, 4          ;Loads the SYSCFG register to R0
	CLR R21, R21, 4              ;Clears bit 4. To enable OCP
	SBCO &R21, C4, 4, 4          ;Writes the change to SYSCFG register
	USERLED0_DISABLE

;----------------------------
; Uses the Breakpoint Macros |
;----------------------------
	BREAKPOINT 42
	DEBUGREG R21


;-----------------------------------------------------------------------------------------------------------------------
; Stop |
;-------
stop:
	HALT


;------------------------------------------------------------
; Resource table section
; Remoteproc needs this section to load the firmware file to the PRU
; It contains only a version number for remoteproc
;-----------------------------------------------------------
	.sect ".resource_table", RW
	.retain                         ;Important to prevent the assembler from removing this section
remoteproc_ResourceTable:
	.word 1, 0, 0 ,0 ,0