;-----------------------------------------------------------------------------------------------------------------------
; Remoteproc Assembler Example |
;-------------------------------
; A basic application to be assembled with clpru.
; The register R30[15] will be toggled with the given DELAY_LOOPS delay until R31[14] will be set to high.
; Delay loop calculation:  (DELAY_LOOPS*5ns)*2=delay in ns.
; The delay loop needs 2 instructions. Therefore the time needs to be multiplied with 2
; Author: Dennis Hofmann
; Date:   2017-11-29
;-----------------------------------------------------------------------------------------------------------------------
; Register usage |
;-----------------
;R30:	General-purpose output interface
;R31:	General-purpose input interface
;R15:	Holds the delay loop counter

	.text
	.global main

DELAY_LOOPS	.set 100000000          ;Waits 1s

main:
	LDI32 R30, 0                    ;Sets all output pins to low

toggle_led_on:
	SET R30, R30, 15                ;Sets PRU-ICSS pin 15 to high -> BBB header pin P8.11
	LDI32 R15, DELAY_LOOPS          ;Loads the delay counter

delay_loop1:                        ;Waits DELAY_LOOPS cycles
	SUB R15, R15, 1
	QBLT delay_loop1, R15, 0

toggle_led_of:
	CLR	R30, R30, 15                ;Clears PRU-ICSS pin 15
	LDI32 R15, DELAY_LOOPS

delay_loop2:                        ;Waits DELAY_LOOPS cycles
	SUB	R15, R15, 1
	QBLT delay_loop2, R15, 0

	QBBS stop, R31, 14              ;Stops if PRU-ICSS pin 14 is high -> BBB header pin P8.16
	QBA toggle_led_on

stop:
	HALT


;------------------------------------------------------------
;Resource table section
;Remoteproc needs this section to load the firmware file to the PRU
;It contains only a version number for remoteproc
;-----------------------------------------------------------
	.sect ".resource_table", RW
	.retain                         ;Important to prevent the assembler from removing this section
remoteproc_ResourceTable:
	.word 1, 0, 0 ,0 ,0
