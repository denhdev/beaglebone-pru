# Shared Memory Usage
An example to use the OCP ports. 
It allows sharing data between the ARM system and the PRU cores. 
Beside message passing its a way to communicate between both systems. 
The PRU core access the ARM system to enable the user LED0. 
The ARM application access the PRU-ICSS to read data from the shared memory.


# Compiling / Assembling
*make* can be used to create the ARM application and the PRU firmware.

    make sharedmem
    make clean -> To clean up


# Install
*make* can be used to copy the firmware to the **/lib/firmware/** directory.

    sudo make install


# Usage
The *start.sh* script can be used to start the PRU core. 

    sudo ./start.sh 

The *remoteproc* interface can be used directly to start the PRU core as shown below.

    echo "none" >/sys/class/leds/beaglebone:green:usr0/trigger
    echo 'start' > /sys/class/remoteproc/remoteproc1/state

After starting the firmware, the ARM application can be used to check if a breakpoint was triggered.

    sudo ./out/main_arm 
