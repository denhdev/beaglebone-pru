# Remote Processor Framework Basic Usage
This example shows how to develop for the PRU-ICSS by using the clpru compiler.
The pin 11 on header 8 will be used as an output pin to toggle a LED. 
The pin 16 on header 8 will be configured as an input pin to shut-down the application. 
This example uses three different development approaches. 
All three have the same logic but different implementations.


# Compiling/Assembling
*make* can be used to create the PRU firmware.

    make asm -> Assemble the asm source
    make c -> Compile the c source
    make casm -> Compile the hybrid version which uses c and asm as source
    make clean -> To clean up


# Installation
*make* can be used to copy the firmware and devicetree overlay to the **/lib/firmware/** directory. 

    sudo make install


# Pin Multiplexing
The *bone_capemgr* can be used to load the devicetree as shown below.

    echo PRU-GPIO-example  > /sys/devices/platform/bone_capemgr/slots

If superuser rights are necessary, the devicetree can be loaded by:

    sudo sh -c "echo PRU-GPIO-example  > /sys/devices/platform/bone_capemgr/slots"


## Checking the Pin Multiplexing

    sudo sh -c "cat /sys/kernel/debug/pinctrl/44e10800.pinmux/pinmux-pins"
    sudo sh -c "cat /sys/kernel/debug/pinctrl/44e10800.pinmux/pins"
    cat /sys/devices/platform/bone_capemgr/slots


# Usage
The *start.sh* script can be used to start the firmware.

    sudo ./start.sh 

The *remoteproc* interface can be used directly to start the PRU core as shown below.

    sudo sh -c "echo 'start' > /sys/class/remoteproc/remoteproc1/state"
    sudo sh -c "echo 'stop' > /sys/class/remoteproc/remoteproc1/state"




