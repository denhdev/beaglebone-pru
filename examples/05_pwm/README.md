# PWM Generator 
A firmware which generates a PWM signal by using a general-purpose output pin. 
This example controls a servo connected to header pin P8.11 of the BeagleBone. 
The user can set the servo angle by entering a value to the terminal. 
Following the input, the C application will store it at the shared memory of the PRU-ICSS and 
trigger an interrupt to signalize the PRU core that reconfiguration is necessary.

# Compiling / Assembling
*make* can be used to create the PRU firmware and the ARM application.
	
	make pwm -> Create the firmware and the ARM application
	make clean -> Clean up


# Install the Firmware
*make* can be used to copy the firmware and overlay to the  **/lib/firmware/** directory.
	
	sudo make install


# Pin Multiplexing
The bone_capemgr can be used to load the devicetree as shown below.

	echo PRU-IO-PWM > /sys/devices/platform/bone_capemgr/slots
	sudo sh -c "echo PRU-IO-PWM > /sys/devices/platform/bone_capemgr/slots"


## Check the Pin multiplexing check

	sudo sh -c "cat /sys/kernel/debug/pinctrl/44e10800.pinmux/pins"
	cat /sys/devices/platform/bone_capemgr/slots

	
# Usage
The *start.sh* script can be used to start the firmware.

	sudo ./start.sh -> Load and start the firmware for PRU0
	
The */sys/class/remoteproc/remoteproc1/state* interface can be used to start the firmware without the script.

    echo 'start' > /sys/class/remoteproc/remoteproc1/state
    echo 'stop' > /sys/class/remoteproc/remoteproc1/state
  
Through the terminal the ARM application can be started by executing:

    ./out/pwm_generator
    
After starting the PRU core waits for an initial configuration. 
The ARM application can be used to configure the PWM generator. 
After setting up the PRU, the PRU core will use the header pin P8.11 as output for the generated signal.

