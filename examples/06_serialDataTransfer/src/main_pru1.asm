;-----------------------------------------------------------------------------------------------------------------------
; GPI Signal Receiving Loop |
;----------------------------
; This firmware listens on the BeagleBone Black pin 26 on header 9 for data.
; The execution stops after receiving the bit order 101.
; The previously captured data will be stored.
; Author: Dennis Hofmann
; Date:   2017-11-29
;-----------------------------------------------------------------------------------------------------------------------
	.text
	.global main
	.retain

ARM_REG_GPIO				.set 0x4804C000			;ARM GPIO Register base address
ARM_REG_GPIO_CLEARDATAOUT	.set 0x190				;CLEARDATAOUT register offset
ARM_REG_GPIO_SETDATAOUT		.set 0x194				;SETDATAOUT register offset

;-----------------------------------------------------------------------------------------------------------------------
;Enables the user LED0. The LED is connected to pin 21
;-----------------------------------------------------------------------------------------------------------------------
USERLED0_ENABLE	.macro
	LDI32 R21, 1<<21                                        ;Loads bit
	LDI32 R20, ARM_REG_GPIO | ARM_REG_GPIO_SETDATAOUT       ;Loads register address
	SBBO &R21, R20, 0, 4                                    ;Enables the pin by writing to ARM_REG_GPIO_SETDATAOUT
	.endm


;-----------------------------------------------------------------------------------------------------------------------
; Disables the user LED 0
;-----------------------------------------------------------------------------------------------------------------------
USERLED0_DISABLE	.macro
	LDI32 R21, 1<<21
	LDI32 R20, ARM_REG_GPIO | ARM_REG_GPIO_CLEARDATAOUT
	SBBO &R21, R20, 0, 4
	.endm


;-----------------------------------------------------------------------------------------------------------------------
; DEBUGREG macro
; Writes a register value to the shared memory and wait for the ARM to read it
;-----------------------------------------------------------------------------------------------------------------------
DEBUGREG   .macro value
;------------------------
; Sets value and bitflag |
;------------------------
	LDI32 R20, 0x10000          ;Loads the memory location
	MOV	R21, value				;Loads the value
	SBBO &R21, R20,	4, 4        ;Stores the value
	LDI32 R21, 01b              ;Sets the bitflag
	SBBO &R21, R20,	0, 4        ;Stores the bitflag
	USERLED0_ENABLE             ;Enables the LED to signilise the break

;--------------------------------------------
; Waits for the ARM to clearing the bitflag |
;--------------------------------------------
$wait?:
	LDI32 R20, 0x10000                         ;Loads the memory location
	LBBO &R1, R20, 0, 1
	QBBS $wait?, R1, 0
	USERLED0_DISABLE
	.endm

;Debug code end
;-----------------------------------------------------------------------------------------------------------------------

SIGNAL_PERIOD   .set 1000        ;Sends the time in ns/5! Send frequency 1/t=f


;-----------------------------------------------------------------------------------------------------------------------
; Serial data receiving loop |
;-----------------------------
; R24: Received data
; R22: Tmp values
; R23: wait cycles
;-------------------------------
main:
;---------------------------------
; Enables OCP for user LED usage |
;---------------------------------
	LBCO &R22, C4, 4, 4			  ;Loads the SYSCFG register
	CLR	R22, R22, 4				  ;Clears the bit 4. to enable OCP
	SBCO &R22, C4, 4, 4			  ;Writes the change to SYSCFG register
	USERLED0_DISABLE

	;Init. receiver
	LDI32 R24, 0b				   ;Clears the data buffer
	LDI32 R22, 0b				   ;Clears tmp
	LDI32 R23, SIGNAL_PERIOD       ;Needs to be even! Otherwise the loop will hangup because of the SUB 2
	SUB R23, R23, 8		           ;Removes the cycles taken by the application beside the wait loop





;checks the input by a given frequency
checkInput:
	QBBS saveHigh, R31, 16			;If input is set, saves high

saveLow:							;Else saves low
	QBA	CheckResult

saveHigh:
	SET	R24, R24, 0					;Saves high on a register by setting a bit

CheckResult:						;Checks for the sync bits
	AND	R22, R24, 111b				;Mask the input. Uses only the first 3 bits.
	XOR	R22, R22, 101b				;Checks for sync value
	LSL	R24, R24, 1					;Shifts 1 bit left after checking
	QBEQ synRec, R22, 0				;A sync signal was detected, if no bit is left

InitWaitLoop:
	MOV	R22, R23					;Loads loop cycles

Wait_loop:							;Waits to check only in a given frequency
	SUB	R22, R22, 2
	QBLT Wait_loop, R22, 0

	QBA checkInput

synRec:
	DEBUGREG R24        ;Writes the received bytes to the shared memory and waits for the ARM


stop:
	HALT





;------------------------------------------------------------
;Resource table section
;Remoteproc needs this section to load the ELF file to the PRU
;It contains only a version number for remoteproc
;-----------------------------------------------------------
	.sect ".resource_table", RW
	.retain                         ;Important to prevent the assembler from removing this section
remoteproc_ResourceTable:
	.word 1, 0, 0 ,0 ,0