/*******************************************************************************
 * Remoteproc assembler and C example  *
 ***************************************
 A basic application to be compiled with clpru.
 The only purposes of this file are to include the resource table and to declare the main function.
 The main will be defined within an assembler file.
 Author: Dennis Hofmann
 Date:   2017-11-29
/*******************************************************************************/
#include "resource_table_empty.h"
extern void main(void);