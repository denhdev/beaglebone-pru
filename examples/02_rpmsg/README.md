# Remote Processor Messaging Framework (RPMsg) Basic Usage
A basic RPMsg example.
It explains a method of message passing between the ARM system and the PRU-ICSS.
The ARM application sends a message to the PRU0 and the PRU0 responses to it. 
The firmware and the ARM application are in C.


## Compiling
*make can be used to compile the PRU firmware and ARM application.

    make rpmsg -> Compiling of the ARM application and the PRU-ICSS firmware
    make clean -> To clean up


## Install
*make* can be used to copy the firmware and overlay to the **/lib/firmware/** directory.

    sudo make install


## Usage
The *start.sh* script can be used to start the firmware.

    sudo ./start.sh 

The ARM application can start the message exchange now

    sudo ./out/msg_example -> Send a message to the PRU and wait for the response
    
The ARM application starts sending messages to the PRU0 core. 
If the PRU0 receives a message, it will respond.
The PRU and ARM will be repeated this exchange several times.

Example terminal output:

    Message 0: Sent 'Hello PRU!' to PRU0
    Message 0 received from PRU0: Hello ARM!
