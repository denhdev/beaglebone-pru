/*******************************************************************************
 * Shared memory ARM application   *
 *************************************
 This ARM application accesses the PRU memory and checks for a set breakpoint flag.
 If the flag is set, the shared memory content will be printed to the terminal.
 After printing, the flag will be cleared.
 Author: Dennis Hofmann
 Date:   2017-11-29
/*******************************************************************************/
#include <stdio.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdint.h>

#define PRU_ADDR 0x4A300000					//Address of the PRU memory on the ARM system
#define SHAREDRAM_OFFSET 0x00010000			//PRU shared RAM address

void main (void) {
	uint32_t* prusharedmemory;
	printf("Read from the PRU-ICSS shared memory\n");

	int fd = open("/dev/mem", O_RDWR | O_SYNC);
	prusharedmemory = (uint32_t*) mmap(NULL, 0x10, PROT_READ | PROT_WRITE, MAP_SHARED, fd, PRU_ADDR + SHAREDRAM_OFFSET);

	if(!prusharedmemory) {
		printf("Can not map shared memory!");
	} else {
		if (prusharedmemory[0] == 1) {
			printf("The PRU0 has reached a breakpoint!\n");
			printf("Message: %d\n", (prusharedmemory[1]));

			prusharedmemory[0] = 0;
		} else {
			printf("Nothing to read!\n");
		}
	}
}